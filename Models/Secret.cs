﻿namespace JournalSecret.Models
{
    public class Secret
    {
        public int Id { get; set; }
        public string Apercu { get; set; }
        public string Description { get; set; }
        public bool Publique { get; set; }
        public string Journal { get; set; }
        public int IdUsager { get; set; }
    }
}
