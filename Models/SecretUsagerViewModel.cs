﻿namespace JournalSecret.Models
{
    public class SecretUsagerViewModel
    {
        public int IdUsager { get; set; }
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public string Alias { get; set; }
        public string Courriel { get; set; }
        public string MotDePasse { get; set; }
        public int IdSecret { get; set; }
        public string Apercu { get; set; }
        public string Description { get; set; }
        public bool Publique { get; set; }
    }
}
