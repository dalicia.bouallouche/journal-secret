﻿using Microsoft.AspNetCore.DataProtection;
using System.Data;
using System.Data.SqlClient;

namespace JournalSecret.Models
{
    public class SecretsDataContext
    {
        private readonly string _connectionString;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SecretsDataContext(string connectionstring, IHttpContextAccessor? httpContextAccessor)
        {
            _connectionString = connectionstring;
            _httpContextAccessor = httpContextAccessor;
        }

        public bool Autoriser(string courriel, string motDePasse)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "SELECT * FROM Usagers WHERE Courriel LIKE '" + courriel + "' AND MotDePasse LIKE '" + motDePasse + "'";
                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;

                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    return true;
                }
                reader.Close();
            }
            return false;
        }
        
        public Tuple<int, string, string, string> GetInfoUsager(string courriel)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "SELECT ID_usager, Prenom, Nom, Alias FROM Usagers WHERE Courriel LIKE '" + courriel + "'";
                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;

                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    int id = Convert.ToInt32(reader["ID_usager"]);
                    string prenom = Convert.ToString(reader["Prenom"]);
                    string nom = Convert.ToString(reader["Nom"]);
                    string alias = Convert.ToString(reader["Alias"]);
                    return new Tuple<int, string, string, string> ( id, prenom, nom, alias);
                }
                reader.Close();
            }
            return new Tuple<int, string, string, string> (-1, "", "", "");
        }

        public List<SecretUsagerViewModel> SelectSecretsPubliques()
        {
            List<SecretUsagerViewModel> listeSecrets = new List<SecretUsagerViewModel>();

            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "select * " + 
                                "from Secrets as S " + 
                                "INNER JOIN Usagers as U " + 
                                "ON U.ID_Usager = S.ID_Usager " + 
                                "Where S.Publique = 1";

                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    SecretUsagerViewModel secretUsager = new SecretUsagerViewModel();
                    secretUsager.Prenom = Convert.ToString(reader["Prenom"]);
                    secretUsager.Nom = Convert.ToString(reader["Nom"]);
                    secretUsager.Alias= Convert.ToString(reader["Alias"]);
                    secretUsager.Apercu = Convert.ToString(reader["Apercu"]);
                    secretUsager.IdUsager = Convert.ToInt32(reader["Id_Usager"]);

                    listeSecrets.Add(secretUsager);
                }
                reader.Close();
            }

            return listeSecrets;
        }
        
        public List<Secret> SelectSecretsUsager()
        {
            List<Secret> listeSecrets = new List<Secret>();

            var courriel = _httpContextAccessor.HttpContext.Request.Cookies["courriel"];


            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {

                int identifianUsager = GetInfoUsager(courriel).Item1;

                string sqlStr = String.Format("select ID_secret, Apercu, Description, Publique, ID_usager " +
                                                "from Secrets " +
                                                "Where ID_usager Like '{0}'", identifianUsager);

                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Secret secret = new Secret();
                    secret.Id = Convert.ToInt32(reader["ID_secret"]);
                    secret.Apercu = Convert.ToString(reader["Apercu"]);
                    secret.Description = Convert.ToString(reader["Description"]);
                    secret.Publique = Convert.ToBoolean(reader["Publique"]);
                    secret.IdUsager = Convert.ToInt32(reader["ID_usager"]);

                    listeSecrets.Add(secret);
                }
                reader.Close();
            }

            return listeSecrets;
        }


        public Secret SelectSecret(int id)
        {
            Secret secret = new Secret();

            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "select ID_secret, Apercu, Description, Publique, ID_usager " +
                                String.Format("from Secrets where ID_secret= {0}", id);

                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;

                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();

                    secret.Id = Convert.ToInt32(reader["ID_secret"]);
                    secret.Apercu = Convert.ToString(reader["Apercu"]);
                    secret.Description = Convert.ToString(reader["Description"]);
                    secret.Publique = Convert.ToBoolean(reader["Publique"]);
                    secret.IdUsager = Convert.ToInt32(reader["ID_usager"]);
                }
                reader.Close();
            }
            return secret;
        }

        public List<SecretUsagerViewModel> RechercherPourPublique(string searchString)
        {
            List<SecretUsagerViewModel> listeSecrets = new List<SecretUsagerViewModel>();

            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = "SELECT U.Prenom, U.Nom, U.Alias, S.Apercu " +
                                "FROM Secrets as S " +
                                "INNER JOIN Usagers as U " +
                                "ON U.ID_Usager = S.ID_Usager " +
                                "WHERE S.Apercu LIKE '%" + searchString + "%'";

                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    SecretUsagerViewModel secretUsager = new SecretUsagerViewModel();
                    secretUsager.Prenom = Convert.ToString(reader["Prenom"]);
                    secretUsager.Nom = Convert.ToString(reader["Nom"]);
                    secretUsager.Alias = Convert.ToString(reader["Alias"]);
                    secretUsager.Apercu = Convert.ToString(reader["Apercu"]);

                    listeSecrets.Add(secretUsager);
                }
                reader.Close();
            }

            return listeSecrets;
        }


        public List<Secret> RechercherPourUsager(string searchString)
        {
            List<Secret> listeSecrets = new List<Secret>();

            var courriel = _httpContextAccessor.HttpContext.Request.Cookies["courriel"];

            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                int identifianUsager = GetInfoUsager(courriel).Item1;

                string sqlStr = String.Format("select ID_secret, Apercu, Description, Publique, ID_usager " +
                                                "from Secrets " +
                                                "Where ID_usager Like {0} and Apercu LIKE '%{1}%'", identifianUsager, searchString);

                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Secret secret = new Secret();
                    secret.Id = Convert.ToInt32(reader["ID_secret"]);
                    secret.Apercu = Convert.ToString(reader["Apercu"]);
                    secret.Description = Convert.ToString(reader["Description"]);
                    secret.Publique = Convert.ToBoolean(reader["Publique"]);
                    secret.IdUsager = Convert.ToInt32(reader["ID_usager"]);

                    listeSecrets.Add(secret);
                }
                reader.Close();
            }

            return listeSecrets;
        }

        public void SupprimerSecret(int id)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = String.Format("delete from Secrets where ID_secret = {0}", id);
                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;

                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void ModifierSecret(Secret secret)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                string sqlStr = String.Format("update Secrets set ID_secret={0}, Apercu='{1}', Description='{2}', Publique={3}, ID_usager={4} " +
                    "WHERE ID_secret={5}", secret.Id, secret.Apercu.Replace("'", "''"), secret.Description.Replace("'", "''"), Convert.ToInt32(secret.Publique), secret.IdUsager, secret.Id);

                SqlCommand cmd = new SqlCommand(sqlStr, connection);
                cmd.CommandType = CommandType.Text;

                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }
}
