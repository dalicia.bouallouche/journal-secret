﻿namespace JournalSecret.Models
{
    public class Usager
    {
        public int Id { get; set; }
        public string Prenom { get; set; }
        public string Nom { get; set; }
        public string Alias { get; set; }
        public string Courriel { get; set; }
        public string MotDePasse { get; set; }
    }
}
