# Projet final
#### Date de remise: le 19 décembre avant minuit.
#### Pondération: 60% de la note totale.

## Partie 1 : Correction de failles de sécurité
### Instructions
Ceci est une application de journal secret (journal intime). Des usagers enregistrés y ont ajouté leurs secrets les plus intimidants. Ceux-ci peuvent êtres publiques ou non. Les secrets publiques seront affichés à des visiteurs du site (non enregistrés). Cette application contient plusieurs vulnérabilités. Parmi celles-ci, certaines permettent de voir la liste complète des secrets (même privés), de télécharger le journal secret d'un membre dont le secret ne devrait pas s'afficher en publique, d'autres permettent de voler des cookies par plusieurs façons, etc. 

 - **Liste des failles à trouver et à corriger**:
	- [ ]  Deux (2) failles XSS reflétées.
	- [ ] Une (1) faille XSS stockée.
	- [ ] Plusieurs cookies sont accessible via du code javascript. 
	- [ ] Trois (3) failles SQLi.
	- [ ] Une (1) faille de type _Unrestricted File Upload_.
	- [ ] Une (1) faille de type _PathTraversal_. **Précision** : vous devez prouver qu'il est possible de télécharger un journal secret d'un usager dont le secret n'est pas visible (n'est pas publique).
	- [ ] Messages d'erreurs trop bavards dans deux (2) pages différentes. Expliquez, dans le billet Git, en quoi c'est problématique.
	 **Notez qu'il est possible de trouver d'autres failles, celles-ci ne seront pas comptabilisées dans vos notes. Uniquement le nombre de failles demandées seront corrigées.**

 - **Réponses aux questions et remise du projet**:
	 Vous devez avoir un projet dans GitLab auquel vous allez m'inviter tant que _Maintainer_. Voici les détails:
	 
	- [ ] Faites un fork de ce projet.

	- [ ] Vous devez avoir une branche avec le code source d'origine (suffixez le nom de votre branche avec __VULN_). **Ne faites aucune modification sur cette branche.**
	- [ ] Vous devez créer un billet pour chaque faille de sécurité. Vous devez utiliser le modèle suivant. 
			
		 ```
		 ## Description
		... Brève description de la faille ...
		## Comment reproduire le bug? (quelques captures d'écran)
		... Étapes de reproduction de la faille (pas besoin de faire toute une attaque complète, une preuve que la vulnérabilité (SQLi, XSS, Path Traversal, etc.) est possbile suffit), avec captures d'écran ...
		1. 
		2. 
		...
		## Comportement observé
		... Que se passe-t-il? Qu'observez-vous? Qu'est-ce qui est problématique ? ...
		## Comportement attendu
		... Qu'est-ce qu'on veut avoir comme résultat à la place du comportement observé (avec le bug)? ...
		## Causes potentielles
		... Dites quelle est la partie de code problématique avec explications, montrez l'emplacement exacte du code source problématique ... 
		## Suggestion de correction
		... Dites, brièvement, comment vous comptez corriger ce bug ...
		 ```

	- [ ] Vous devez corriger chaque faille reportée dans le billet de bug. 
	- [ ] **Important**: La correction du bug doit être liée au billet. Pour cela, vous devez écrire dans votre message de commit la mention _Fixes #num_Issue_ (fonctionne aussi avec close, closes, closed, fixed à la place de Fixes). Ceci permettra de fermer le billet et de le lier à votre correctif. Pour plus de détails voir https://docs.github.com/en/issues/tracking-your-work-with-issues/linking-a-pull-request-to-an-issue. 

**Notes en vrac**
- Il est claire que les validations de données de formulaires (client et serveur) peuvent corriger plusieurs des failles mentionnées ci-dessus. Cependant, ceci ne sera pas une solution suffisante pour celles-ci. Ce qui est demandé c'est de corriger la faille spécifique. Vous pouvez évidemment faire la validation de données client et serveur mais celle-ci ne sera pas comptabilisée dans la note.
- Les failles SQLi dans les deux (2) endroits où il y a la recherche sont considérées comme étant la même faille.

## Partie 2 : tests d'intrusion et rapport de  une application de tests existante sera utilisée. L'énoncée sera dans un autre document.
