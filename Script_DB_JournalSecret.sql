use master 
GO

IF db_id('JournalSecret') IS NOT NULL 
 alter database [JournalSecret] set single_user with rollback immediate
 GO
 DROP DATABASE [JournalSecret]
GO

CREATE DATABASE [JournalSecret]
GO

USE [JournalSecret]
GO
/****** Object:  Table [Secrets]    Script Date: 2022-11-29 21:13:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Secrets](
	[ID_secret] [int] NOT NULL,
	[Apercu] [varchar](max) NULL,
	[Description] [varchar](max) NULL,
	[Publique] [bit] NULL,
	[ID_usager] [int] NULL,
 CONSTRAINT [PK_Secrets] PRIMARY KEY CLUSTERED 
(
	[ID_secret] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Usagers]    Script Date: 2022-11-29 21:13:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Usagers](
	[ID_usager] [int] NOT NULL,
	[Prenom] [varchar](50) NULL,
	[Nom] [varchar](50) NULL,
	[Alias] [varchar](50) NULL,
	[Courriel] [varchar](100) NULL,
	[MotDePasse] [nvarchar](max) NULL,
 CONSTRAINT [PK_Usagers] PRIMARY KEY CLUSTERED 
(
	[ID_usager] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [Secrets] ([ID_secret], [Apercu], [Description], [Publique], [ID_usager]) VALUES (1, N'Je m''abonne aux AABA (Addicts Aux Bonbons Anonymes)', N'Je m''abonne aux AAB anonymes (Addicts Aux Bonbons anonymes)', 0, 1)
INSERT [Secrets] ([ID_secret], [Apercu], [Description], [Publique], [ID_usager]) VALUES (2, N'Je d�teste No�l.. et les sapins.', N'Je d�teste No�l et les sapins.', 0, 2)
INSERT [Secrets] ([ID_secret], [Apercu], [Description], [Publique], [ID_usager]) VALUES (3, N'Je dois ralentir sur la bi�re...', N'Je dois ralentir sur la bi�re.', 1, 3)
INSERT [Secrets] ([ID_secret], [Apercu], [Description], [Publique], [ID_usager]) VALUES (4, N'Je me suis abonn� aux AA (alcooliques anonymes).', N'Je me suis abonn� aux AA (alcooliques anonymes).', 0, 3)
INSERT [Secrets] ([ID_secret], [Apercu], [Description], [Publique], [ID_usager]) VALUES (5, N'J''ai encore abus� de mon autorit�!', N'J''ai encore abus� de mon autorit�!', 1, 4)
INSERT [Secrets] ([ID_secret], [Apercu], [Description], [Publique], [ID_usager]) VALUES (6, N'R�solution 2023: �tre plus organis�.', N'R�solution 2023: �tre plus organis�.', 1, 5)
INSERT [Secrets] ([ID_secret], [Apercu], [Description], [Publique], [ID_usager]) VALUES (7, N'Je d�teste les haricots sous toutes les formes, ma m�re ne le comprend pas.', N'Je d�teste les haricots sous toutes les formes, ma m�re ne le comprend pas.', 1, 6)
INSERT [Secrets] ([ID_secret], [Apercu], [Description], [Publique], [ID_usager]) VALUES (8, N'J''abandonne mes cours, je n''aime pas la prog.', N'J''abandonne mes cours, je n''aime pas la prog.', 0, 7)
INSERT [Secrets] ([ID_secret], [Apercu], [Description], [Publique], [ID_usager]) VALUES (9, N'Je veux �tre footballeur (joueur de soccer).', N'Je veux �tre footballeur (joueur de soccer)', 1, 7)
INSERT [Secrets] ([ID_secret], [Apercu], [Description], [Publique], [ID_usager]) VALUES (10, N'J''arr�te de faire semblant de tomber et de me rouler par terre.', N'J''arr�te de faire semblant de tomber et de me rouler par terre.', 1, 7)
GO
INSERT [Usagers] ([ID_usager], [Prenom], [Nom], [Alias], [Courriel], [MotDePasse]) VALUES (1, N'Guy', N'Mauve', N'Guimauve', N'Guimauve@gmail.com', N'Password1')
INSERT [Usagers] ([ID_usager], [Prenom], [Nom], [Alias], [Courriel], [MotDePasse]) VALUES (2, N'Christ', N'Mastree', N'ChristmasTree', N'Christmas@gmail.com', N'Password1')
INSERT [Usagers] ([ID_usager], [Prenom], [Nom], [Alias], [Courriel], [MotDePasse]) VALUES (3, N'Gerard', N'Mensoif', N'GRarementsoif', N'GerardMensoif@gmail.com', N'Password2')
INSERT [Usagers] ([ID_usager], [Prenom], [Nom], [Alias], [Courriel], [MotDePasse]) VALUES (4, N'Agathe', N'Thepower', N'IGotThepower', N'AgatheThepower@gmail.com', N'123456')
INSERT [Usagers] ([ID_usager], [Prenom], [Nom], [Alias], [Courriel], [MotDePasse]) VALUES (5, N'Alain', N'Proviste', N'Alimproviste', N'AlainProviste@gmail.com', N'123456')
INSERT [Usagers] ([ID_usager], [Prenom], [Nom], [Alias], [Courriel], [MotDePasse]) VALUES (6, N'Harry', N'Covert', N'HaricotVert', N'HarryCovert@gmail.com', N'password')
INSERT [Usagers] ([ID_usager], [Prenom], [Nom], [Alias], [Courriel], [MotDePasse]) VALUES (7, N'Jean', N'Neymar', N'Jenaimarre', N'JeanNeymar@gmail.com', N'password')
GO
ALTER TABLE [Secrets]  WITH CHECK ADD  CONSTRAINT [FK_Secrets_Usagers] FOREIGN KEY([ID_usager])
REFERENCES [Usagers] ([ID_usager])
GO
ALTER TABLE [Secrets] CHECK CONSTRAINT [FK_Secrets_Usagers]
GO