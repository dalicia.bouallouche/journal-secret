﻿using JournalSecret.Models;
using Microsoft.AspNetCore.Mvc;

namespace JournalSecret.Controllers
{
    public class ConnexionController : Controller
    {
        private readonly SecretsDataContext _dtContext;

        public ConnexionController(SecretsDataContext dtContext)
        {
            this._dtContext = dtContext;
        }

        [HttpGet]
        public IActionResult Connexion()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Connexion(string courriel, string motDePasse)
        {
            bool autorise = _dtContext.Autoriser(courriel, motDePasse);
            if (autorise)
            {
                this.Response.Cookies.Append("courriel", courriel);

                motDePasse = string.IsNullOrEmpty(motDePasse) ? "" : motDePasse;

                this.Response.Cookies.Append("motDePasse", motDePasse);

                return RedirectToAction("ListeSecretsUsager", "Secret");
            }

            if (_dtContext.GetInfoUsager(courriel).Item1 == -1)
            {
                TempData["MessageErreur"] = String.Format("L'usager {0} n'existe pas.", courriel);
            }
            else
            {
                TempData["MessageErreur"] = String.Format("Erreur de mot de passe pour l'usager {0}.", courriel);
            }
            
            return View();
        }
    }
}
