﻿using JournalSecret.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting.Internal;
using System.Linq;
using System.Reflection;

namespace JournalSecret.Controllers
{
    public class FileIOController : Controller
    {
        private readonly IFileProvider _fileProvider;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public FileIOController(IFileProvider provider, IWebHostEnvironment hostingEnvironment)
        {
            _fileProvider = provider;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult TelechargerFichier(int IdUsager)
        {
            // On cherche le nom du fichier ayant l'identifiant IdUsager
            string fileName = "";
            foreach (var item in _fileProvider.GetDirectoryContents("Files"))
            {
                string subFileName = "Journal" + IdUsager.ToString();
                if (item.Name.Contains(subFileName))
                {
                    fileName = item.Name;
                    break;
                }
            }

            var filePath = Path.Combine(_hostingEnvironment.WebRootPath, "Files", fileName);

            var memoryStream = new MemoryStream();

            try
            {
                using (var stream = new FileStream(filePath, FileMode.Open))
                {
                    stream.CopyTo(memoryStream);
                }

                memoryStream.Position = 0;
                return File(memoryStream, "text/txt", Path.GetFileName(filePath));
            }
            catch (Exception)
            {

                return RedirectToAction("ListeSecretsPublique", "Secret");
            }

        }

        [HttpPost]
        public IActionResult TeleverserFichier(IFormFile fichier, int IdUsager)
        {
            string fileName = "Journal" + IdUsager + fichier.FileName;

            if (fichier != null)
            {
                string filePath = Path.Combine(_hostingEnvironment.WebRootPath, "Files", fileName);

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    fichier.CopyTo(stream);
                }
            }

            TempData["filePath"] = Path.Combine(_hostingEnvironment.WebRootPath, "Files", fileName);

            return RedirectToAction("ListeSecretsUsager", "Secret");
        }
    }
}
