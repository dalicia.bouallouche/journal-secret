﻿using JournalSecret.Models;
using Microsoft.AspNetCore.Mvc;

namespace JournalSecret.Controllers
{
    public class SecretController : Controller
    {
        private readonly SecretsDataContext _dtContext;

        public SecretController(SecretsDataContext dtContext)
        {
            this._dtContext = dtContext;
        }

        [HttpGet]
        public ActionResult ListeSecretsPublique(string searchString)
        {
            List<SecretUsagerViewModel> listeSecrets = this._dtContext.SelectSecretsPubliques();


            if (!String.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                listeSecrets = this._dtContext.RechercherPourPublique(searchString);
                ViewBag.searchString = searchString;
            }

            return View(listeSecrets);
        }

        [HttpGet]
        public ActionResult ListeSecretsUsager(string searchString, int id)
        {
            List<Secret> listeSecrets = this._dtContext.SelectSecretsUsager();


            if (!String.IsNullOrEmpty(searchString))
            {
                searchString = searchString.ToLower();
                listeSecrets = this._dtContext.RechercherPourUsager(searchString);
                ViewBag.searchString = searchString;
            }


            if (id != 0)
            {
                ViewBag.identifiant = id;
            }

            string courriel = HttpContext.Request.Cookies["courriel"];
            Tuple<int, string, string, string> infoUsager = this._dtContext.GetInfoUsager(courriel);

            ViewBag.IdUsager = infoUsager.Item1;
            ViewBag.Prenom = infoUsager.Item2;
            ViewBag.Nom = infoUsager.Item3;
            ViewBag.Alias = infoUsager.Item4;

            return View(listeSecrets);
        }

        public ActionResult Editer(int id)
        {
            Secret secret = this._dtContext.SelectSecret(id);
            return View(secret);
        }

        [HttpPost]
        public ActionResult Editer(Secret secret)
        {
            this._dtContext.ModifierSecret(secret);
            return RedirectToAction(nameof(ListeSecretsUsager));
        }
    }
}
